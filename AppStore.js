import { createStore, combineReducers } from 'redux'
import UserReducer from './reducers/UserReducer'
import BookingReducer from './reducers/BookingReducer';

const reducers = combineReducers({
    user: UserReducer,
    booking: BookingReducer
})

const store = createStore(reducers)
const state = store.getState();
export default store