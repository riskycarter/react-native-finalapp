import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Linking, ImageBackground, Button, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { InputItem, WhiteSpace, Icon } from '@ant-design/react-native';
import moment from 'moment';
import axios from 'axios'

import Nav from '../components/NavBar'
import BackBar from '../components/GoBackBar'

class MoviePage extends Component {
    state = {
        movieID: '',
        movie: '',
        date: new Date().getTime(),
        movieShowTimes: '',
        isLoading: true,
    }

    UNSAFE_componentWillMount() {
        this.setState({
            movie:
                this.props.location.state.movie,
            movieShowTimes:
                this.props.location.state.showTimes
        })

        console.log(this.props)
        console.log('hhhhhhhhhhh ',this.state.date)
        console.log('hello movie ', this.props.location.state.movie)
        console.log('hello movie state ', this.state.movie)
        console.log('hello movie showTimes ', this.props.location.state.showTimes)
        this.setState({ isLoading: false })
        // axios.get('http://54.158.232.52:8888/showtime/movie/' + this.props.location.state.movie.id, {
        //     params:
        //     {
        //         date: this.state.date.getTime()
        //     }

        // })
        //     .then(response => response.data)
        //     .then(data => {
        //         this.setState({
        //             movieShowTimes: data
        //         })
        //         console.log('showtimes movie ', this.state.movieShowTimes)
        //         console.log('showtimes movie ', this.state.movieShowTimes, ' indexxxxxxxx ', this.state.movieShowTimes[0])

        //         this.setState({ isLoading: false })
        //     })


    }

    goToCinemaPage = (item) => {
        this.props.history.push('/cinema', {
            cinema: item,

        })
    }

    render() {


        return (
            <View style={styles.container}>

                <BackBar style={{ flex: 1 }} ></BackBar>

                <View style={{ flex: 2, alignItems: 'center', marginTop: 14 }}>
                    <Image
                        style={{ width: 150, height: 200 }}
                        source={{ uri: this.state.movie.imageUrl }}
                    />
                    <Text style={{ color: 'white', fontSize: 20 }}>{this.state.movie.name}</Text>
                    <Text style={{ color: 'white', fontSize: 16 }}>{}</Text>

                    <FlatList
                        style={{ width: '100%' }}
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.movieShowTimes}
                        renderItem={({ item }) =>
                            <View style={{ borderBottomWidth: 1, borderColor: 'rgba(255, 198, 11, 0.8)', marginTop: 4 }}>
                                <Text style={{ color: '#bbbbbb' }}>Sound  {item.movie.soundtrack.toUpperCase()} | Subtitle  {item.movie.soundtrack.toUpperCase} | {moment(item.startDateTime).format('DD/MM/YYYY')}</Text>
                                {this.state.date <= item.startDateTime ? (
                                    <TouchableOpacity style={{
                                        padding: 10,
                                        backgroundColor: 'rgba(192,192,192,0.2)',
                                        borderRadius: 5,
                                        margin: 10,

                                    }}
                                        onPress={() => {
                                            this.goToCinemaPage(item)
                                        }}
                                    >
                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <Text style={{ color: 'white' }}>{moment(item.startDateTime).format('HH:mm')} - {moment(item.endDateTime).format('HH:mm')} </Text>
                                            <View style={{ flex: 2, alignItems: "flex-end" }}>
                                                <Icon name="right" />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                ) : (
                                        <TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.2)',
                                            borderRadius: 5,
                                            margin: 10,

                                        }}
                                            disabled={true}
                                            onPress={() => {
                                                this.goToCinemaPage(item)
                                            }}
                                        >
                                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                                <Text style={{ color: '#bbbbbb' }}>{moment(item.startDateTime).format('HH:mm')} - {moment(item.endDateTime).format('HH:mm')} </Text>
                                                <View style={{ flex: 2, alignItems: "flex-end" }}>
                                                    <Icon name="right" />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    )}

                            </View>
                        }

                    />
                </View>

                <View style={{}}>
                    <Nav style={{}}></Nav>
                </View>
                {/* </ImageBackground> */}
            </View>
        )
    }
}


export default MoviePage

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    backgroundImage: {
        flex: 1,
        color: 'rgba(0, 0, 0, 0.5)'

    },



})
