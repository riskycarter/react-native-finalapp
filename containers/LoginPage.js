import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Linking, ImageBackground, Button, TouchableOpacity } from 'react-native';
import { InputItem, WhiteSpace, } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';

class LoginPage extends Component {
    state = {
        username: 'newnew',
        password: '1111',
        firstName: '',
        lastName: '',
        token: '',
        image: undefined,
        isCorrect: false,
        isShowThing: false,
        memberId: ''
    }

    onValueChange = (field, value) => {
        this.setState({ [field]: value });
    }

    componentWillMount() {

    }

    onSubmitLogin = async () => {
        // console.log('arai waa? ', this.state.email)
        // try {
            // axios.get('http://54.158.232.52:8888/movies')
            // .then(response => response.data)
            // .then(data => {
            //     console.log("new data : ",data)
                
            // })
            // .catch(error => {
            //     console.log('error change password ', error.response)
            // })

            const response = await axios.get('http://54.158.232.52:8888/members')
            console.log("new login : ", response)
            for(let each of response.data){
                console.log("for loop response : ", each.email, each.password)
                if(this.state.email === each.email){
                    if(this.state.password === each.password){
                        this.setState({ isCorrect: true,
                            memberId: each.id,
                            firstName: each.firstName,
                            lastName: each.lastName,
                            image: each.image,
                            password: each.password
                         })
                    }
                }
            }
            // if (response.data.user.image === undefined) {
            //     this.setState({
            //         token: response.data.user.token,
            //         firstName: response.data.user.firstName,
            //         lastName: response.data.user.lastName
            //     })
            // }

        //     const response2 = await axios.get('https://zenon.onthewifi.com/ticGo/users', {
        //         headers:
        //         {
        //             Authorization: 'Bearer '.concat(response.data.user.token)
        //         }
        //     })
        //     if (response2.data.user.image !== undefined) {
        //         this.setState({
        //             image: response2.data.user.image
        //         })
        //     }
        //     this.setState({ isCorrect: true })
        //     console.log('arai waa? ', response.data)


        // } catch (error) {
        //     console.log('error: ', error);
        // }

        if (this.state.isCorrect) {
            this.setState({ isShowThing: false })
            const userInfor = {
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                token: 'token',
                image: this.state.image,
                memberId: this.state.memberId
            }
            this.props.setUser(userInfor)
            this.props.history.replace('/movies')
        } else {
            this.setState({ isShowThing: true })
        }
    }

    asGuest = () => {
        const userInforGuest = {
            email: 'guest',
            password: 'guest',
            firstName: 'Guest',
            lastName: 'Guest',
            token: ''
        }
        this.props.setUser(userInforGuest)
        this.props.history.replace('/movies')
    }


    render() {

        return (
            <View style={styles.container}>
                <ImageBackground
                    style={styles.backgroundImage}
                    source={{ uri: 'https://i.pinimg.com/originals/08/c3/06/08c3064c404182c03bb4ec90482d2d6d.jpg' }}
                >
                    <View style={styles.inputForm}>
                        <View style={{ width: 115, height: 120 }}>
                            <Image
                                style={{ width: '100%', height: '100%' }}
                                source={{ uri: 'https://seatgeek.com/images/superbowl/53/tickets.png' }}
                            />
                        </View>
                        <View style={styles.inputText} >
                            <Text style={{ marginTop: 20, color: '#ffc60b' }}>E-mail</Text>
                            <InputItem
                                clear
                                placeholder="example@gmail.com"
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                style={{ color: 'white' }}
                                onChangeText={value => { this.onValueChange("email", value.toLowerCase()) }}
                            />
                            <Text style={{ marginTop: 20, color: '#ffc60b' }}>Password</Text>
                            <InputItem
                                clear
                                placeholder="•••••"
                                ref={el => (this.inputRef = el)}
                                type="password"
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                style={{ color: 'white' }}
                                onChangeText={value => { this.onValueChange("password", value) }}
                            />
                            {this.state.isShowThing ? <Text style={{ color: 'red' }}>Invalid email or password</Text> : null}
                        </View>
                        <View style={{ marginTop: 40, }}>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 100,
                            }}
                                onPress={() => {
                                    this.onSubmitLogin()
                                }}>
                                <Text style={{
                                    color: '#ffc60b',
                                    textAlign: 'center',
                                    fontFamily: 'Roboto'
                                }}>SIGN IN</Text>
                            </TouchableOpacity>

                        </View>
                        <Text style={{ color: '#bbbbbb', textDecorationLine: 'underline', marginTop: 20, padding: 14 }}
                            onPress={() =>
                                this.props.history.push('/signup')} >
                            Sign up
                    </Text>
                        <Text style={{ color: '#bbbbbb', padding: 14, margin: 20 }}
                            onPress={() =>
                                this.asGuest()
                            } >
                            Skip login
                    </Text>
                    </View>
                </ImageBackground>
            </View>
        )
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (userInfor) => {
            dispatch({
                type: 'SET_CURRENT_USER',
                email: userInfor.email,
                password: userInfor.password,
                firstName: userInfor.firstName,
                lastName: userInfor.lastName,
                token: userInfor.token,
                image: userInfor.image,
                memberId: userInfor.memberId
            })
        }
    }
}


export default connect(null, mapDispatchToProps)(LoginPage)

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1
    },
    container: {
        flex: 1,
        backgroundColor: '#444444',
    },

    inputForm: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputText: {
        width: '65%'
    },

})
