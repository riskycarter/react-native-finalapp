import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Linking, ImageBackground, Button, TouchableOpacity } from 'react-native';
import { InputItem, WhiteSpace, Toast, portal, } from '@ant-design/react-native';
import axios from 'axios';



class RegisterPage extends Component {

    state = {
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        isCorrect: false,
        isShowThing: false
    }

    UNSAFE_componentWillMount() {

    }
    onValueChange = (field, value) => {
        this.setState({ [field]: value });
    }

    checkInputForm = async () => {
        try {

            if ((this.state.firstName && this.state.lastName && this.state.email && this.state.password) !== '' ) {
                this.setState({ isShowThing: false })
                this.setState({ isCorrect: true })
                 await axios.post('http://54.158.232.52:8888/register', {
                    email: this.state.email,
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    image: undefined,
                    password: this.state.password,
                })
                this.props.history.push('/login')
            } else {
                console.log('sdasdsadasdjwidjqwoijdqwoijqdwoi')
                this.setState({ isShowThing: true })
            }




        } catch (error) {
            console.log('error: ', error.response.data.errors);
        }


    }



    render() {

        return (
            <View style={styles.container}>
                <ImageBackground
                    style={styles.backgroundImage}
                    source={{ uri: 'https://i.pinimg.com/originals/08/c3/06/08c3064c404182c03bb4ec90482d2d6d.jpg' }}
                >
                    <View style={[styles.inputForm]}>
                        <View style={[styles.inputText]} >
                            <View style={{ borderWidth: 3, borderColor: '#ffc60b', marginBottom: 8 }}>
                                <Text style={{ fontSize: 24, color: '#ffc60b', textAlign: 'center' }}> SIGN UP</Text>
                            </View>
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>E-mail</Text>
                            <InputItem
                                clear
                                placeholder="example@gmail.com"
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                style={{ color: 'white' }}
                                // value='test2@gmail.com'

                                // value={this.state.email}
                                onChange={email => {
                                    this.setState({
                                        email : email.toLowerCase()
                                    });
                                }}

                            />
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>Password</Text>
                            <InputItem
                                clear
                                placeholder="•••••"
                                ref={el => (this.inputRef = el)}
                                type="password"
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                // value={this.state.password}
                                // value='123456'
                                style={{ color: 'white' }}
                                onChange={password => {
                                    this.setState({
                                        password
                                    });
                                }}

                            />
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>First Name</Text>
                            <InputItem
                                clear
                                placeholder=""
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                style={{ color: 'white' }}
                                // value={this.state.firstName}
                                onChange={firstName => {
                                    this.setState({
                                        firstName
                                    });
                                }}
                            />
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>Last Name</Text>
                            <InputItem
                                clear
                                placeholder=""
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                // value={this.state.lastName}
                                style={{ color: 'white' }}
                                onChange={lastName => {
                                    this.setState({
                                        lastName
                                    });
                                }}
                            />
                            {this.state.isShowThing ? <Text style={{color:'red'}}>please fill all textboxes</Text>:null}
                        </View>
                        <View style={{ marginTop: 30, }}>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 100,
                            }}
                                onPress={() => {
                                    this.checkInputForm()
                                }}
                            >
                                <Text style={{
                                    color: '#ffc60b',
                                    textAlign: 'center'
                                }}>SIGN UP</Text>
                            </TouchableOpacity>

                        </View>
                        <Text style={{ color: '#bbbbbb', textDecorationLine: 'underline', marginTop: 10, padding: 14 }}
                            onPress={() => this.props.history.push('/login')}>
                            Back
                    </Text>
                    </View>
                </ImageBackground>
            </View>
        )
    }

}

export default RegisterPage

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1
    },
    container: {
        flex: 1,
        backgroundColor: '#444444',
    },

    inputForm: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputText: {
        width: '65%'
    },

})