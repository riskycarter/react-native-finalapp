import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, ImageBackground, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux'
import { InputItem, WhiteSpace, Icon } from '@ant-design/react-native';
import Nav from '../components/NavBar'
import moment from 'moment';

import axios from 'axios'

class ProfilePage extends Component {

    state = {
        isLoading: true,
        firstName: '',
        lastName: '',
        token: '',
        histories: '',
        allShowTimes: '',
        sumPrices: [],
        movieNames: [],
        isGetProduct: false,
    }


    UNSAFE_componentWillMount() {
        this.setState({ token: this.props.user.token })
        console.log('data of user TOKEN ', this.props.user.token)
        console.log('data of user FIRSTNAME ', this.props.user.firstName)
        if (this.props.user.token !== '') {
            axios.get('http://54.158.232.52:8888/booking/memberId',
                {
                    params: {
                        memberId: this.props.user.memberId
                    }
                })
                .then(response => response.data)
                .then(data => {
                    console.log("new data : ", data)
                    this.setState({ histories: data, isLoading: false })
                })
                .catch(error => {
                    console.log('error change password ', error.response)
                })
        }
        // if (this.props.user.token !== '') {
        //     axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
        //         headers:
        //         {
        //             Authorization: 'Bearer '.concat(this.props.user.token)
        //         }
        //     })
        //         .then(response => response.data)
        //         .then(data => {
        //             this.setState({ histories: data })
        //             this.state.histories.reverse()
        //             console.log('data of user ', data)
        //             return data
        //         })
        //         .catch(error => {
        //             console.log('get history ', error)
        //         })
        //         .then(data => {
        //             data.forEach((element, index) => {
        //                 console.log(element.showtime)
        //                 axios.get('https://zenon.onthewifi.com/ticGo/movies/showtimes/' + element.showtime)
        //                     .then(response => response.data)
        //                     .then(data => {
        //                         console.log('forEach ', data)
        //                         this.setState({
        //                             movieNames: {
        //                                 ...this.state.movieNames,
        //                                 [index]: data.movie.name
        //                             },
        //                             isGetProduct: true
        //                         })

        //                     })
        //             });
        //             return data
        //         }
        //         )
        //     this.setState({ isLoading: false })
        // }
    }

    getSum = (history) => {
        let total = 0
        for (let arr of history.seats) {
            total = total + arr.price;
        }
        console.log('total : ', total)
        return total
    }


    render() {

        console.log('data of histories ', this.state.histories)
        console.log('image ', this.props.user.image)
        const { user } = this.props
        if (this.props.user.token !== '') {
            // if (this.state.isGetProduct) {
            return (
                <View style={{ flex: 1 }}>
                    <ImageBackground
                        style={styles.backgroundImage}
                        source={{ uri: 'https://i.pinimg.com/originals/08/c3/06/08c3064c404182c03bb4ec90482d2d6d.jpg' }}
                    >
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>

                                {this.props.user.image === undefined ||this.props.user.image === null ? <Image
                                    style={{ width: 120, height: 120, borderRadius: 5, marginTop: 20, marginLeft: 20 }}
                                    source={{ uri: 'https://www.img.in.th/images/19cfe1295150d78b2607d5ab7f59baa9.png' }}
                                /> : null}
                                {this.props.user.image !== undefined && this.props.user.image !== null ? <Image
                                    style={{ width: 120, height: 120, borderRadius: 5, marginTop: 20, marginLeft: 20 }}
                                    source={{ uri: this.props.user.image }}
                                /> : null}


                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                    <Text style={{ fontSize: 20, color: 'white', fontFamily: 'Iowan Old Style', marginTop: 20, marginLeft: 10 }} >{user.firstName} {user.lastName}</Text>


                                    <View style={{ marginLeft: 10,marginTop: 2 }}>
                                        <TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.5)',
                                            borderRadius: 5,
                                            width: 100,
                                        }}
                                            onPress={() => {
                                                this.props.history.push('/editprofile')
                                            }}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center',
                                                fontFamily: 'Roboto'
                                            }}>Edit Profile</Text>
                                        </TouchableOpacity>

                                        <View style={{ marginTop: 10 }}>
                                            <TouchableOpacity style={{
                                                padding: 10,
                                                backgroundColor: 'rgba(192,192,192,0.5)',
                                                borderRadius: 5,
                                                width: 160,
                                            }}
                                                onPress={() => {
                                                    this.props.history.push('/changepassword')
                                                }}>
                                                <Text style={{
                                                    color: 'white',
                                                    textAlign: 'center',
                                                    fontFamily: 'Roboto'
                                                }}>Change Password</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                            </View>

                        </View>


                        <View style={{ flex: 2, height: 260 }}>
                        <Text style={{ fontSize: 20, color: '#bbbbbb', marginBottom: 8 }} > History ( {this.state.histories.length} ) </Text>
                            {this.state.histories !== '' && !this.state.isLoading ? (
                                <FlatList
                                    style={{ width: '100%' }}
                                    data={this.state.histories}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item, index }) =>
                                        <View style={{ marginLeft: 4, marginBottom: 4 }}>
                                            <View style={{ borderBottomWidth: 1, borderColor: 'rgba(255, 198, 11, 0.8)' }}>
                                                <Text style={{ color: '#bbbbbb', marginTop: 4 }}>#{index + 1} {item.showTime.movie.name}  {moment(item.bookingTime).format('DD/MM/YYYY HH:mm')} </Text>
                                                <View>
                                                    <Text style={{ color: '#bbbbbb', marginLeft: 6 }} >
                                                        [ {this.state.histories[index].showTime.cinema[0].name ? this.state.histories[index].showTime.cinema[0].name : null} -->
                                                    {moment(item.showTime.startDateTime).format('DD/MM/YYYY HH:mm')} ]
                                                    </Text>
                                                </View>
                                                {this.state.histories[index].seats.map((i) => {
                                                    return (<View>
                                                        <Text style={{ color: '#bbbbbb', marginLeft: 6 }}> row {i.rowSeat} column {i.columnSeat} | {i.seatType} | {i.price} ฿</Text>
                                                    </View>
                                                    )
                                                })}

                                                <View style={{marginTop: 2 }}>
                                                    <Text style={{ color: '#bbbbbb', marginLeft: 6 }}> TOTAL PRICE : {
                                                        this.getSum(this.state.histories[index])
                                                    } ฿</Text>
                                                </View>
                                                <View>
                                                    <Text style={{ color: '#bbbbbb', marginLeft: 6 }}></Text>
                                                </View>
                                            </View>

                                        </View>
                                    }

                                />
                            ) : <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.8)' }}>
                                    <ActivityIndicator style={{ flex: 1 }} size="large" color="#0000ff" />
                                </View>}



                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ color: '#bbbbbb', textDecorationLine: 'underline', marginTop: 10, padding: 14 }}
                                onPress={() =>
                                    this.props.history.replace('/login')
                                } >
                                Sign out
                                </Text>
                        </View>
                    </ImageBackground>
                    <Nav></Nav>
                </View>


            )

        }
        if (this.props.user.token === '') {
            return (
                <View style={{ flex: 1, backgroundColor: 'black' }}>
                    <View style={{ flex: 1 }}>
                        <View style={{ alignItems: 'center' }}>
                            <View >
                                <Image
                                    style={{ width: 120, height: 120, borderRadius: 5, marginTop: 20 }}
                                    source={{ uri: 'https://www.img.in.th/images/19cfe1295150d78b2607d5ab7f59baa9.png' }}
                                />
                            </View>

                            <Text style={{ fontSize: 30, color: 'white', fontFamily: 'Iowan Old Style' }} >{user.firstName} {user.lastName}</Text>

                            <Text style={{ color: '#bbbbbb', textDecorationLine: 'underline', marginTop: 20, padding: 14 }}
                                onPress={() =>
                                    this.props.history.replace('/login')
                                } >
                                Sign out
                                </Text>
                        </View>

                    </View>
                    <Nav style={{}}></Nav>
                </View>

            )
        }
        return (<View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.8)' }}>
            <ActivityIndicator style={{ flex: 1 }} size="large" color="#0000ff" />
            <View>
                <Nav>
                </Nav>
            </View>
        </View>
        )
    }

}
const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps)(ProfilePage)

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1
    },
    textContent: {
        // color: 'white'
    }
})
