import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Linking, ImageBackground, Button, TouchableOpacity, FlatList, ActivityIndicator, Dimensions } from 'react-native';
import { InputItem, WhiteSpace, Icon } from '@ant-design/react-native';
import moment from 'moment';
import axios from 'axios'
import ImageZoom from 'react-native-image-pan-zoom';
import PinchZoomView from 'react-native-pinch-zoom-view';
import { connect } from 'react-redux'

import Nav from '../components/NavBar'
import BackBar from '../components/GoBackBar'

class CinemaPage extends Component {
    state = {
        isLoading: true,
        cinema: {},
        seats: [],
        newSeats: [],
        seatCinemas: [],
        currentSeat: {},
        currentRowS: [],
        currentRowP: [],
        currentRowD: [],
        currentColumn: '',
        reserveSeats: [],
        isShowThing: false,
        isReserved: false,
        isSelectedS: false,
        showTimeID: '',
        listSeats: [],
        movie: {},
        isLoading: true
    }
    componentDidMount() {
    }

    _keyExtractorSeats = (item, index) => item._id

    UNSAFE_componentWillMount() {

        axios.get('http://54.158.232.52:8888/showtime/' + this.props.location.state.cinema.id)
            .then(response => response.data)
            .then(data => {
                console.log("new data : ", data)
                this.setState({
                    cinema: data.cinema[0],
                    seats: data.seats,
                    newSeats: data.seats,
                    showTimeID: data.id,
                    movie: data.movie,
                    seatCinemas: data.cinema[0].seatCinemas
                })
                this.setState({ isLoading: false })
            })
        // this.setState({
        //     cinema:
        //         this.props.location.state.cinema.cinema[0],
        //     seats:
        //         this.props.location.state.cinema.seats,
        //     newSeats: this.props.location.state.cinema.seats,
        //     showTimeID:
        //         this.props.location.state.cinema.id,
        //     movie: this.props.location.state.cinema.movie,
        //     seatCinemas: this.props.location.state.cinema.cinema[0].seatCinemas
        // })
        // console.log('cinema ', this.props.location.state.cinema.cinema[0].name)
        // console.log(this.props.location.state.cinema)
        // console.log('showtime Id  ', this.props.location.state.cinema.id)


    }

    onSelectSeat = (item) => {
        let a = this.state.newSeats.slice(); //creates the clone of the state
        var indexSeat = this.state.newSeats.findIndex(i => {
            return i.id === item.id
        })
        console.log("zzzzzzzz ", a)
        if (indexSeat != -1) {
            a[indexSeat].reserved = !item.reserved;
            this.setState({ newSeats: a });
            console.log("changed!!  ", this.state.seats[indexSeat])
        }
        if (this.state.reserveSeats.length !== 0) {

            var indexReserve = this.state.reserveSeats.findIndex(i => {
                return item.id === i.item.id
            })
            console.log("findindex : ", indexReserve)
            if (indexReserve != -1) {
                this.state.reserveSeats.splice(indexReserve, 1)

            } else {
                this.state.reserveSeats.push({ item })
            }
        } else {
            this.state.reserveSeats.push({ item })
        }




        this.setState({ isShowThing: true, isSelectedS: !this.state.isSelectedS })
        this.props.addBookingList(this.state.reserveSeats)
        console.log('SET RESERVE S E A T > ', this.state.reserveSeats)
        console.log('isSelected  > ', this.state.isSelectedS)


    }


    render() {
        if (!this.state.isLoading) {
            if (this.state.cinema !== {}) {
                return (
                    <View style={styles.container}>
                        <BackBar style={{ flex: 1 }} ></BackBar>
                        <View style={{ flex: 1 }}>
                            <View style={{}}>
                                <View style={{ margin: 6 }}>
                                    <Text style={{ fontSize: 18, textAlign: 'center', color: '#ffc60b' }} > {this.state.movie.name}  {moment(this.props.location.state.cinema.startDatetime).format('DD/MM/YYYY')} </Text>
                                </View>
                                <View style={{ margin: 6 }}>
                                    <Text style={{ fontSize: 16, textAlign: 'center', color: '#ffc60b' }} > {this.state.cinema.name}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 3, borderColor: '#ffc60b' }}></View>
                                <View style={{ margin: 10 }}>
                                    <Text style={{ fontSize: 20, textAlign: 'center', color: 'white' }}>SCREEN</Text>
                                </View>

                                <ScrollView style={{ height: 260, width: '100%' }}>
                                    <View style={{ flex: 1, width: "100%", alignItems: 'center' }}>
                                        {this.state.seatCinemas.map((i => {
                                            return (
                                                <FlatList
                                                    data={this.state.newSeats}
                                                    numColumns={i.columnsSeat}
                                                    keyExtractor={this._keyExtractorSeats}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    renderItem={({ item, index }) => {
                                                        if (item.seatType === i.seatType) {
                                                            if (item.seatType === "SOFA" && !item.reserved) {

                                                                return (
                                                                    <View style={{ margin: 6 }}>
                                                                        <TouchableOpacity onPress={() =>
                                                                            this.onSelectSeat(item)
                                                                        }>
                                                                            <View>
                                                                                <Image style={{ width: 26, height: 20 }} source={require('../imgs/sofaS.png')} />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                )

                                                            } else if (item.seatType === "PREMIUM" && !item.reserved) {
                                                                return (
                                                                    <View style={{ margin: 4 }}>
                                                                        <TouchableOpacity onPress={() =>
                                                                            this.onSelectSeat(item)
                                                                        }>
                                                                            <View>
                                                                                <Image
                                                                                    style={{ width: 20, height: 20 }}
                                                                                    source={require('../imgs/sofaP.png')}
                                                                                />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                )
                                                            } else if (item.seatType === "DELUXE" && !item.reserved) {
                                                                return (
                                                                    <View style={{ margin: 4 }}>
                                                                        <TouchableOpacity onPress={() =>
                                                                            this.onSelectSeat(item)
                                                                        }>
                                                                            <View>
                                                                                <Image
                                                                                    style={{ width: 20, height: 20 }}
                                                                                    source={require('../imgs/sofaD.png')}
                                                                                />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                )
                                                            } else if (item.reserved) {
                                                                return (
                                                                    <View style={{ margin: 4 }}>
                                                                        <TouchableOpacity onPress={() =>
                                                                            this.onSelectSeat(item)
                                                                        }>
                                                                            <View>
                                                                                <Image
                                                                                    style={{ width: 20, height: 20 }}
                                                                                    source={require('../imgs/reserved.png')}
                                                                                />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                    </View>

                                                                )
                                                            } else {
                                                                <View style={{ margin: 2 }}>
                                                                    <Image
                                                                        style={{ width: 18, height: 18 }}
                                                                        source={require('../imgs/reserved.png')}
                                                                    />
                                                                </View>
                                                            }
                                                        }
                                                    }
                                                    }
                                                />
                                            )
                                        }))}
                                    </View>
                                </ScrollView>

                                <View style={{ marginTop: 20, alignItems: 'center' }}>
                                    <Text style={{}}>
                                        {this.state.seatCinemas.map((i, index) => {
                                            if (i.seatType === "SOFA") {
                                                return (
                                                    <Text>
                                                        <Image
                                                            style={{ width: 24, height: 24 }}
                                                            source={require('../imgs/sofaS.png')}
                                                        />
                                                        <Text style={{ color: '#ffc60b' }}>{i.type}
                                                            <Text style={{ color: '#bbbbbb' }}>{i.price}฿ </Text>
                                                        </Text>
                                                    </Text>
                                                )
                                            }
                                            if (i.seatType === "PREMIUM") {
                                                return (
                                                    <Text>
                                                        <Image
                                                            style={{ width: 24, height: 24 }}
                                                            source={require('../imgs/sofaP.png')}
                                                        />
                                                        <Text style={{ color: '#ffc60b' }}>{i.type}
                                                            <Text style={{ color: '#bbbbbb' }}>{i.price}฿ </Text>
                                                        </Text>
                                                    </Text>

                                                )
                                            }
                                            if (i.seatType === "DELUXE") {
                                                return (
                                                    <Text>
                                                        <Image
                                                            style={{ width: 24, height: 24 }}
                                                            source={require('../imgs/sofaD.png')}
                                                        />
                                                        <Text style={{ color: '#ffc60b' }}>{i.type}
                                                            <Text style={{ color: '#bbbbbb' }}>{i.price}฿ </Text>
                                                        </Text>
                                                    </Text>
                                                )
                                            }

                                        })}
                                    </Text>

                                </View>
                                <View style={{ marginTop: 20, alignItems: 'center' }}>
                                    {this.props.user.token !== '' ?
                                        (this.state.reserveSeats.length!==0 ?<TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.5)',
                                            borderRadius: 5,
                                            width: 100,
                                        }}
                                            onPress={() => {
                                                this.props.history.push('/bookinglist', {
                                                    showTimeID: this.props.location.state.cinema.id,
                                                    movie: this.props.location.state.cinema.movie,
                                                    reserveSeats: this.state.reserveSeats
                                                })
                                            }}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center',
                                                fontFamily: 'Roboto'
                                            }}>Reserve</Text>
                                        </TouchableOpacity> : 
                                        <TouchableOpacity style={{
                                            padding: 10,
                                            borderRadius: 5,
                                            width: 150,
                                        }}
                                        disabled={true}
                                            onPress={() => {
                                                this.props.history.push('/bookinglist', {
                                                    showTimeID: this.props.location.state.cinema.id,
                                                    movie: this.props.location.state.cinema.movie,
                                                    reserveSeats: this.state.reserveSeats
                                                })
                                            }}>
                                            <Text style={{
                                                color: '#bbbbbb',
                                                textAlign: 'center',
                                                fontFamily: 'Roboto'
                                            }}>Please select Seat</Text>
                                        </TouchableOpacity> )
                                         :

                                        <TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.5)',
                                            borderRadius: 5,
                                            width: 140,
                                        }}
                                            onPress={() => {
                                                this.props.history.replace('/login')
                                            }}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center',
                                                fontFamily: 'Roboto'
                                            }}>Please Sign In</Text>
                                        </TouchableOpacity>}
                                </View>
                                {/* { this.state.isShowThing ? <Text style={{color:'red'}}>{this.state.reserveSeats}</Text>:null} */}
                            </View>
                        </View>

                        <Nav ></Nav>
                    </View>
                )
            }
        }

        return (<View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.8)' }}>
            <View>
                <BackBar style={{ backgroundColor: 'black' }}></BackBar>
            </View>
            <ActivityIndicator style={{ flex: 1 }} size="large" color="#0000ff" />
            <View>
                <Nav>
                </Nav>
            </View>
        </View>
        )


    }

}
const mapStateToProps = (state) => {
    return {
        user: state.user,
        booking: state.booking
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addBookingList: (bookingList) => {
            dispatch({
                type: 'ADD_BOOKING',
                bookingList: bookingList
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CinemaPage)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'black',
    },



})