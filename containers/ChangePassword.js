import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, ImageBackground, TouchableOpacity, Button } from 'react-native';
import { connect } from 'react-redux'
import { InputItem, WhiteSpace, Icon } from '@ant-design/react-native';
import Nav from '../components/NavBar'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'

class EditProfilePage extends Component {

    state = {
        firstName: '',
        lastName: '',
        oldPassword: '',
        newPassword: '',
        inputOldPassword: '',
        isShowThing: false

    }


    UNSAFE_componentWillMount() {
        this.setState({
            oldPassword: this.props.user.password,
        })
    }

    onValueChange = (field, value) => {

        this.setState({ [field]: value });
        console.log('newwwwwwwwwww ', this.state.newPassword)
    }

    saveEdit = () => {
        if (this.state.oldPassword === this.state.newPassword || this.state.newPassword !== '' || this.state.inputOldPassword !== '') {
            this.setState({ isShowThing: true })
        }
        if (this.state.oldPassword !== this.state.inputOldPassword) {
            this.setState({ isShowThing: true })
        }


        if (this.state.oldPassword === this.state.inputOldPassword) {
            this.setState({ isShowThing: false })
            axios.put('http://54.158.232.52:8888/changepassword/' + this.props.user.memberId,
                {
                    password: this.state.newPassword
                }
            )
                .then(response => {
                    console.log('response ', response)
                })
                .catch(error => {
                    console.log('error change password ', error.response)
                })
            this.setState({ isShowThing: false })

            this.props.setUser(this.state.newPassword)
            this.props.history.replace('/login')
        } else {
            this.setState({ isShowThing: true })

        }
    }

    render() {
        const { user } = this.props
        return (

            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ImageBackground
                    style={styles.backgroundImage}
                    source={{ uri: 'https://i.pinimg.com/originals/08/c3/06/08c3064c404182c03bb4ec90482d2d6d.jpg' }}
                >

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>


                        <View style={{ width: '65%' }} >
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>Old Password</Text>
                            <InputItem
                                clear
                                placeholder=''
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                style={{ color: 'white' }}
                                onChangeText={value => { this.onValueChange("inputOldPassword", value) }}
                            />
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>New Password</Text>
                            <InputItem
                                clear
                                placeholder=''
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                style={{ color: 'white' }}
                                onChangeText={value => { this.onValueChange("newPassword", value) }}
                            />
                            {this.state.isShowThing ? <Text style={{ color: 'red' }}>Please enter new password, make sure the new password != old password  </Text> : null}
                        </View>
                        <View style={{ marginTop: 40, }}>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 100,
                            }}
                                onPress={() => {
                                    this.saveEdit()
                                }}>
                                <Text style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontFamily: 'Roboto'
                                }}>Save</Text>
                            </TouchableOpacity>

                        </View>
                        <Text style={{ color: '#bbbbbb', textDecorationLine: 'underline', marginTop: 20, padding: 14 }}
                            onPress={() =>
                                this.props.history.replace('/profile')} >
                            Back
                    </Text>

                    </View>

                </ImageBackground>
                <Nav style={{}}></Nav>
            </View>



        )
    }

}
const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (userInfor) => {
            dispatch({
                type: 'EDIT_PASSWORD',
                password: userInfor
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfilePage)

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1
    },
})
