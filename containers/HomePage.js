import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Button, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Tabs, WhiteSpace } from '@ant-design/react-native';
import { withRouter } from "react-router";
import moment from 'moment';
import axios from 'axios'
import Nav from '../components/NavBar'

 class HomePage extends React.Component {
    state = {
        isLoading: true,
        date: new Date(),
        currentDate: Date(),
        today: new Date(),
        tomorrow: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
        theDayAfterTomorrow: new Date(new Date().getTime() + 48 * 60 * 60 * 1000),
        products: [],
        isGetProduct: false,
        showTimes: [],
        showTimesTmr: [],
        showTimesTmrTmr: [],
        movieID: '',
        movie: ''
    }
    componentDidMount() {

    }

    UNSAFE_componentWillMount() {
        this.state.today.setHours(7)
        this.state.tomorrow.setHours(7)
        this.state.theDayAfterTomorrow.setHours(7)
        console.log('today ',this.state.today)
        console.log('today ',this.state.today.getTime())
        this.setState({
            today: this.state.today.getTime(),
            currentDate: moment(this.state.currentDate).format('DD-MM-YYYY'),
            tomorrow: this.state.tomorrow.getTime(),
            theDayAfterTomorrow: this.state.theDayAfterTomorrow.getTime(),
        })
        console.log("--------------------")
        console.log(this.state.tomorrow.setHours(7))
        console.log(this.state.tomorrow.setMinutes(0))
        console.log(this.state.tomorrow)
        console.log(this.state.tomorrow.getTime())
        console.log(moment(this.state.tomorrow).add(1, 'day').format('DD-MM-YYYY'))

        // axios.get('http://54.158.232.52:8888/movies' )
        //     .then(response => response.data)
        //     .then(data => {
        //         console.log("new data : ",data)
        //         this.setState({ products: data })
        //         this.setState({ isLoading: false, isGetProduct: true })
        //     })


        axios.get('http://54.158.232.52:8888/movies')
            .then(response => response.data)
            .then(data => {
                this.setState({ products: data })
                console.log("new data : ",data)
                return data
            })
            .catch(error => {
                console.log(error)
            })
            .then(data => {
                data.forEach((element, index) => {
                    console.log(element.id)
                    axios.get('http://54.158.232.52:8888/showtime/movie/' + element.id , {
                        params:
                        {
                            date: this.state.today
                        }
                    })
                        .then(response => response.data)
                        .then(data => {
                            console.log('forEach data ', data)
                            console.log('forEachhhhhhhhhhhhhh1 ', this.state.date.getTime())
                            console.log('forEachhhhhhhhhhhhhh2 ', data.startDateTime)
                            console.log('forEachhhhhhhhhhhhhh length ', data.length)

                            this.setState({
                                showTimes: {
                                    ...this.state.showTimes,
                                    [index]: data
                                }
                            })


                            console.log('showtimes ', this.state.showTimes, ' index ', index)
                        })
                });
                return data
            }
            )
            .then(data => {
                data.forEach((element, index) => {
                    console.log(element.id)
                    axios.get('http://54.158.232.52:8888/showtime/movie/' + element.id, {
                        params:
                        {
                            date: this.state.tomorrow
                        }

                    })
                        .then(response => response.data)
                        .then(data => {
                            console.log('forEach ', data)
                            this.setState({
                                showTimesTmr: {
                                    ...this.state.showTimesTmr,
                                    [index]: data
                                }
                            })
                            console.log('showtimes T M R ', this.state.showTimesTmr, ' index ', index)
                        })
                });
                return data


            }

            )
            .then(data => {
                data.forEach((element, index) => {
                    console.log(element.id)
                    axios.get('http://54.158.232.52:8888/showtime/movie/' + element.id, {
                        params:
                        {
                            date: this.state.theDayAfterTomorrow
                        }

                    })
                        .then(response => response.data)
                        .then(data => {
                            console.log('forEach ', data)
                            this.setState({
                                showTimesTmrTmr: {
                                    ...this.state.showTimesTmrTmr,
                                    [index]: data
                                }
                                , isGetProduct: true
                            })
                            console.log('showtimes T M R T M R ', this.state.showTimesTmrTmr, ' index ', index)
                        })

                    this.setState({ isLoading: false })
                });


            }

            )

    }


    goToMovieShowtime = (item, showTimes) => {
        this.props.history.push('/movie', {
            movie: item,
            showTimes: showTimes

        })
    }



    render() {

        const renderContent1 = () => {

            const style = {
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                flex: 1,
                borderRadius: 5
            };
            const content1 = this.state.products.map((i, index) => {
                return (
                    <View key={`_${i.name}`} style={style}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
                            <View >
                                <Image style={{ width: 150, height: 200, borderRadius: 5 }} source={{ uri: i.imageUrl }} />
                            </View>
                            <View style={{ flex: 2, marginTop: 10, }}>
                                <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', color: 'white' }}>
                                    {i.name}
                                </Text>
                                <View style={{ marginLeft: 16 }}>
                                    <Text style={{ lineHeight: 26, paddingTop: 10, fontSize: 16, color: '#bbbbbb' }}>
                                        Duration : {i.duration + " minutes"} {'\n'}
                                        Soundtrack : {i.soundtrack + '  '} {'\n'}
                                        Subtitle : {i.subtitle + ' '} {'\n'}
                                        Showtime :
                                        {
                                            this.state.showTimes && this.state.showTimes[index] ?
                                                this.state.showTimes[index].length
                                                : 0
                                        }
                                    </Text>
                                    {this.state.showTimes[index].length!==0? (<TouchableOpacity style={{
                                        padding: 10,
                                        backgroundColor: 'rgba(192,192,192,0.5)',
                                        width: 140,
                                        borderRadius: 5
                                    }}
                                    onPress={() => {
                                        this.goToMovieShowtime(i, this.state.showTimes[index])
                                    }}
                                       >
                                      
                                        <Text style={{
                                            color: 'white',
                                            textAlign: 'center'
                                        }}>View Showtimes</Text>
                                    </TouchableOpacity>):(
                                        <TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.5)',
                                            width: 140,
                                            borderRadius: 5
                                        }}
                                        disabled={true}
                                        onPress={() => {
                                            this.goToMovieShowtime(i, this.state.showTimes[index])
                                        }}
                                           >
                                          
                                            <Text style={{
                                                color: '#bbbbbb',
                                                textAlign: 'center'
                                            }}>No Showtime </Text>
                                        </TouchableOpacity>
                                    )}
                                    

                                </View>
                            </View>
                        </View>
                    </View>

                );
            })
            return <ScrollView style={{ backgroundColor: 'black' }}>{content1}</ScrollView>;
        };

        const renderContent2 = () => {
            const style = {
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                flex: 1,
                borderRadius: 5
            };

            const content2 = this.state.products.map((i, index) => {
                console.log('renderContent2 najaaa ', this.state.showTimesTmr)
                return (
                    <View key={`_${i.name}`} style={style}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
                            <View >
                                <Image style={{ width: 150, height: 200, borderRadius: 5 }} source={{ uri: i.imageUrl }} />
                            </View>
                            <View style={{ flex: 2, marginTop: 10, }}>
                                <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', color: 'white' }}>
                                    {i.name}
                                </Text>
                                <View style={{ marginLeft: 16 }}>
                                    <Text style={{ lineHeight: 26, paddingTop: 10, fontSize: 16, color: '#bbbbbb' }}>
                                        Duration : {i.duration + " minutes"} {'\n'}
                                        Soundtrack : {i.soundtrack + '  '} {'\n'}
                                        Subtitle : {i.subtitle + ' '} {'\n'}
                                        Showtime : 
                                        {
                                            this.state.showTimesTmr && this.state.showTimesTmr[index] ?
                                                this.state.showTimesTmr[index].length
                                                : 0
                                        }
                                    </Text>
                                    {this.state.showTimesTmr[index].length!==0? (<TouchableOpacity style={{
                                        padding: 10,
                                        backgroundColor: 'rgba(192,192,192,0.5)',
                                        width: 140,
                                        borderRadius: 5
                                    }}
                                    onPress={() => {
                                        this.goToMovieShowtime(i, this.state.showTimesTmr[index])
                                    }}
                                       >
                                      
                                        <Text style={{
                                            color: 'white',
                                            textAlign: 'center'
                                        }}>View Showtimes</Text>
                                    </TouchableOpacity>):(
                                        <TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.5)',
                                            width: 140,
                                            borderRadius: 5
                                        }}
                                        disabled={true}
                                        onPress={() => {
                                            this.goToMovieShowtime(i, this.state.showTimesTmr[index])
                                        }}
                                           >
                                          
                                            <Text style={{
                                                color: '#bbbbbb',
                                                textAlign: 'center'
                                            }}>No Showtime </Text>
                                        </TouchableOpacity>
                                    )}

                                </View>
                            </View>
                        </View>
                    </View>
                );
            });
            return <ScrollView style={{ backgroundColor: 'black' }}>{content2}</ScrollView>;
        };
        const renderContent3 = () => {
            const style = {
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                flex: 1,
                borderRadius: 5
            };
            const content3 = this.state.products.map((i, index) => {
                console.log('renderContent3 najaaa ', this.state.showTimesTmr)
                return (
                    <View key={`_${i.name}`} style={style}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
                            <View >
                                <Image style={{ width: 150, height: 200, borderRadius: 5 }} source={{ uri: i.imageUrl }} />
                            </View>
                            <View style={{ flex: 2, marginTop: 10, }}>
                                <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', color: 'white' }}>
                                    {i.name}
                                </Text>
                                <View style={{ marginLeft: 16 }}>
                                    <Text style={{ lineHeight: 26, paddingTop: 10, fontSize: 16, color: '#bbbbbb' }}>
                                        Duration : {i.duration + " minutes"} {'\n'}
                                        Soundtrack : {i.soundtrack + '  '} {'\n'}
                                        Subtitle : {i.subtitle + ' '} {'\n'}
                                        Showtime :
                                        {
                                            this.state.showTimesTmrTmr && this.state.showTimesTmrTmr[index] ?
                                                this.state.showTimesTmrTmr[index].length
                                                : 0
                                        }
                                    </Text>
                                    {this.state.showTimesTmrTmr[index].length!==0? (<TouchableOpacity style={{
                                        padding: 10,
                                        backgroundColor: 'rgba(192,192,192,0.5)',
                                        width: 140,
                                        borderRadius: 5
                                    }}
                                    onPress={() => {
                                        this.goToMovieShowtime(i, this.state.showTimesTmrTmr[index])
                                    }}
                                       >
                                      
                                        <Text style={{
                                            color: 'white',
                                            textAlign: 'center'
                                        }}>View Showtimes</Text>
                                    </TouchableOpacity>):(
                                        <TouchableOpacity style={{
                                            padding: 10,
                                            backgroundColor: 'rgba(192,192,192,0.5)',
                                            width: 140,
                                            borderRadius: 5
                                        }}
                                        disabled={true}
                                        onPress={() => {
                                            this.goToMovieShowtime(i, this.state.showTimesTmrTmr[index])
                                        }}
                                           >
                                          
                                            <Text style={{
                                                color: '#bbbbbb',
                                                textAlign: 'center'
                                            }}>No Showtime </Text>
                                        </TouchableOpacity>
                                    )}

                                </View>
                            </View>
                        </View>
                    </View>
                );
            });
            return <ScrollView style={{ backgroundColor: 'black' }}>{content3}</ScrollView>;
        };


        const tabs2 = [
            { title: this.state.currentDate },
            { title: moment(this.state.tomorrow).format('DD-MM-YYYY') },
            { title: moment(this.state.theDayAfterTomorrow).format('DD-MM-YYYY') },
        ];

        if (!this.state.isLoading) {
            if (this.state.isGetProduct) {
                return (
                    <View style={{ flex: 2 }}>
                        <Tabs tabBarBackgroundColor="black"
                            tabBarActiveTextColor='#ffc60b'
                            tabBarInactiveTextColor='#bbbbbb'
                            tabBarUnderlineStyle={{ backgroundColor: '#ffc60b' }}
                            tabs={tabs2} initialPage={0} tabBarPosition="top">

                            {renderContent1}

                            {renderContent2}

                            {renderContent3}

                        </Tabs>
                        <Nav>
                        </Nav>
                    </View>
                )
            }
        }
        return (<View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.8)' }}>
            <ActivityIndicator style={{ flex: 1 }} size="large" color="#0000ff" />
            <View>
                <Nav>
                </Nav>
            </View>
        </View>
        )



    }
}


export const title = 'Tabs';
export const description = 'Tabs example';
export default withRouter(HomePage)

const styles = StyleSheet.create({
    textContent: {

    }

})
