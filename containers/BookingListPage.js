import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, ImageBackground, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux'
import { InputItem, WhiteSpace, Icon } from '@ant-design/react-native';
import Nav from '../components/NavBar'
import moment from 'moment';

import axios from 'axios'

import BackBar from '../components/GoBackBar'

class BookingListPage extends Component {
    state = {
        reserveSeats: '',
        reserveSeatsForSend: '',
        showtimeID: '',
        movie: {},
        cinema: '',
        price: '',
        totalPrice: ''
    }

    UNSAFE_componentWillMount() {
        this.setState({
            reserveSeats: this.props.booking.bookingList,
            showtimeID: this.props.location.state.showTimeID,
            movie: this.props.location.state.movie
        })
        var a = []
        if (this.props.booking.bookingList !== 0) {
            this.props.booking.bookingList.map((i, index) => {
                console.log("inside mapping : ", i)
                var id = i.item.id
                return a.push({ id })
            })
            this.setState({ reserveSeatsForSend: a })
        }
        // console.log("send seatOriginal : ", this.state.reserveSeats)
        // console.log("send seat : ", this.state.reserveSeatsForSend)
        console.log('REducerrrrrrrrrr : ', this.props.booking.bookingList)
        console.log(this.props)
        console.log('showtimeID  ', this.props.location.state.showTimeID)
        console.log('current token ', this.props.user)
    }

    onClickCancel = () => {
        this.setState({ reserveSeats: [] })
        this.props.addBookingList(this.state.reserveSeats)
        this.props.history.goBack()
    }

    onClickConfirm = () => {
        console.log("send seat send : ", this.state.reserveSeatsForSend)
        console.log("send seat : ", this.state.reserveSeats)

        axios.post('http://54.158.232.52:8888/book/' + this.props.user.memberId,
            {
                showtimeId: this.props.location.state.showTimeID,
                seats: this.state.reserveSeatsForSend
            }

        )
            .catch(error => {
                console.log('error booking... ', error.response)
            })

        this.setState({ reserveSeats: [] })
        this.props.history.replace('/movies')
    }

    getSum = () => {
        let total = 0
        let currentPrice = 0
        for (let seat of this.state.reserveSeats) {

            total = total + seat.item.price
        }

        console.log('total : ', total)
        console.log('reserveSeats : ', this.state.reserveSeats)
        return total
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                <View style={{ flex: 1 }}>
                    <Text style={{ color: 'white', fontSize: 20, textAlign: 'center', marginTop: 20, marginBottom: 20 }}>Booking List</Text>
                    {this.state.reserveSeats !== '' ?
                        (<FlatList
                            style={{ width: '100%' }}
                            data={this.state.reserveSeats}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) =>
                                <View style={{ marginLeft: 4, marginBottom: 4 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderColor: 'rgba(255, 198, 11, 0.8)' }}>
                                        <View>
                                            <Image
                                                style={{ width: 95, height: 120, borderRadius: 5, marginTop: 20, marginLeft: 20, marginBottom: 20 }}
                                                source={{ uri: this.state.movie.imageUrl }}
                                            />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', marginTop: 30, marginLeft: 10 }}>
                                            <Text style={{ color: '#bbbbbb', fontSize: 18 }}>{this.state.movie.name}</Text>
                                            <Text style={{ color: '#bbbbbb', fontSize: 16, marginTop: 3,marginLeft: 3 }} >{item.item.seatType}</Text>
                                            <Text style={{ color: '#bbbbbb', marginTop: 3,marginLeft: 3 }}>-> row {item.item.rowSeat} | column {item.item.columnSeat}</Text>
                                            <Text style={{ color: '#bbbbbb', marginTop: 3,marginLeft: 3 }}>{item.item.price} ฿</Text>
                                            {/* {item.seatType === "SOFA" ? (<Text style={{ color: '#bbbbbb' }}>500฿</Text>) :
                                                (item.seatType === "PREMIUM" ? (<Text style={{ color: '#bbbbbb' }}>180฿</Text>) :
                                                    (<Text style={{ color: '#bbbbbb' }}>120฿</Text>))} */}
                                        </View>
                                    </View>
                                </View>
                            }
                        />) : null}

                    <View style={{ alignItems: 'center' }}>
                        <View>
                            <Text style={{ color: '#bbbbbb', marginBottom: 10 }}> TOTAL PRICE : {
                                this.getSum()
                            } ฿</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 100,
                            }}
                                onPress={() => {
                                    this.onClickConfirm()
                                }}>
                                <Text style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontFamily: 'Roboto'
                                }}>Confirm</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 100,
                                marginLeft: 10
                            }}
                                onPress={() => {
                                    this.onClickCancel()
                                }}>
                                <Text style={{
                                    color: 'red',
                                    textAlign: 'center',
                                    fontFamily: 'Roboto'
                                }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <Nav style={{}} ></Nav>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        booking: state.booking
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addBookingList: (bookingList) => {
            dispatch({
                type: 'ADD_BOOKING',
                bookingList: bookingList
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingListPage)