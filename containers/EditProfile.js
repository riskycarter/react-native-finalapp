import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, ImageBackground, TouchableOpacity, Button } from 'react-native';
import { connect } from 'react-redux'
import { InputItem, WhiteSpace, Icon } from '@ant-design/react-native';
import Nav from '../components/NavBar'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'

class EditProfilePage extends Component {

    state = {
        firstName: '',
        lastName: '',
        imagePath: '',
        imageURI: undefined,
        isShowThing: false,
        isClickSelectImage: false,
        oldPassword: '',
        newPassword: '',
        pleaseWait: false

    }

    selectImage = () => {

        ImagePicker.showImagePicker({}, (response) => {
            console.log('responseseeeeeee uri ', response)
            if (response.uri) {
                const formData = new FormData()
                formData.append('file', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('http://54.158.232.52:8888/uploadFile', formData,
                    {
                        file: formData,
                        onUploadProgress: progressEvent => {
                            console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                        }
                    }
                   

                )
                    .then(response => {
                        this.setState({
                            imageURI: response.data,
                            image: response.data
                        })
                        console.log('response after post uploadfile : ', response)
                        console.log('response after post uploadfile : ', response.data)
                    })
                    .catch(error => {
                        console.log('error upload : ', error.response)
                    })
              
            }
        })
    }

    UNSAFE_componentWillMount() {
        this.setState({
            firstName: this.props.user.firstName,
            lastName: this.props.user.lastName,
            image: this.props.user.image,
        })
    }


    saveEdit = () => {
        if (this.state.firstName && this.state.lastName !== '') {
            axios.put('http://54.158.232.52:8888/editprofile/' + this.props.user.memberId, {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                image: this.state.image
            }
            )
                .then(response => {

                })
                .catch(error => {
                    console.log('error edit profile ', error.response)
                })
            this.setState({ isShowThing: false })
            const userInfor = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                image: this.state.image
            }
            this.props.setUser(userInfor)
            this.props.history.replace('/profile')
        } else {
            this.setState({ isShowThing: true })

        }
    }

    render() {
        const { user } = this.props
        return (

            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ImageBackground
                    style={styles.backgroundImage}
                    source={{ uri: 'https://i.pinimg.com/originals/08/c3/06/08c3064c404182c03bb4ec90482d2d6d.jpg' }}
                >

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View >
                            {/* {this.props.user.image === undefined ? <Image
                                style={{ width: 120, height: 120, borderRadius: 5, marginTop: 20 }}
                                source={{ uri: 'https://www.img.in.th/images/19cfe1295150d78b2607d5ab7f59baa9.png' }}
                            /> : null} */}
                            {this.state.imageURI !== undefined ? <Image
                                style={{ width: 120, height: 120, borderRadius: 5, marginTop: 20 }}
                                source={{ uri: this.state.imageURI }}
                            /> : null}
                            {this.props.user.image && this.state.imageURI === undefined ? <Image
                                style={{ width: 120, height: 120, borderRadius: 5, marginTop: 20 }}
                                source={{ uri: this.props.user.image }}
                            /> : null}
                        </View>
                        <View style={{ marginTop: 20, }}>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 140,
                            }}

                                onPress={this.selectImage}>
                                <Text style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontFamily: 'Roboto'
                                }}>Select Image</Text>
                            </TouchableOpacity>

                        </View>

                        <View style={{ width: '65%' }} >
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>First Name</Text>
                            <InputItem
                                clear
                                placeholder=''
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                style={{ color: 'white' }}
                                value={this.state.firstName}
                                onChange={firstName => {
                                    this.setState({
                                        firstName
                                    });
                                }}
                            />
                            <Text style={{ marginTop: 10, color: '#ffc60b' }}>Last Name</Text>
                            <InputItem
                                clear
                                placeholder=''
                                ref={el => (this.inputRef = el)}
                                placeholderTextColor='#bbbbbb'
                                color='white'
                                value={this.state.lastName}
                                style={{ color: 'white' }}
                                onChange={lastName => {
                                    this.setState({
                                        lastName
                                    });
                                }}
                            />
                            {this.state.isShowThing ? <Text style={{ color: 'red' }}>Please fill all textboxes</Text> : null}
                        </View>
                        <View style={{ marginTop: 40, }}>
                            <TouchableOpacity style={{
                                padding: 10,
                                backgroundColor: 'rgba(192,192,192,0.5)',
                                borderRadius: 5,
                                width: 100,
                            }}
                                onPress={() => {
                                    this.saveEdit()
                                }}>
                                <Text style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontFamily: 'Roboto'
                                }}>Save</Text>
                            </TouchableOpacity>

                        </View>
                        <Text style={{ color: '#bbbbbb', textDecorationLine: 'underline', marginTop: 20, padding: 14 }}
                            onPress={() =>
                                this.props.history.replace('/profile')} >
                            Back
                    </Text>

                    </View>

                </ImageBackground>
                <Nav style={{}}></Nav>
            </View>



        )
    }

}
const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (userInfor) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: userInfor.firstName,
                lastName: userInfor.lastName,
                image: userInfor.image
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfilePage)

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1
    },
    textContent: {
        // color: 'white'
    }
})
