export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_BOOKING':
            return {
                bookingList: action.bookingList
            }

        default:
            return state
    }
}




