export default (state = {}, action) => {
    switch (action.type) {
        case 'SET_CURRENT_USER':
            return {
                email: action.email,
                password: action.password,
                firstName: action.firstName,
                lastName: action.lastName,
                token: action.token,
                image: action.image,
                memberId: action.memberId
            }
        case 'EDIT_USER':
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
                image: action.image,
            }
            case 'EDIT_PASSWORD':
            return {
                password: action.password
            }

        default:
            return state
    }
}