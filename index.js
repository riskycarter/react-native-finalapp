/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './Router';
// import App from './containers/EditProfile';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
