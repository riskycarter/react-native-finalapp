import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Linking, ImageBackground, TouchableOpacity } from 'react-native';
import { InputItem, Flex, Button, WingBlank, Icon } from '@ant-design/react-native';
import { withRouter } from "react-router-native";

class GoBackBar extends Component {

    render() {

        return (
            <View  >
                <Flex direction="row" style={{ backgroundColor: 'rgba(0, 0, 0, 0.1)' }} >
                    <Flex.Item style={{ padding: 10 }} >
                        <View >
                            <TouchableOpacity style={{
                                width: '40%',
                                height: 30,
                                justifyContent: 'center',
                            }}
                                onPress={() => {
                                    this.props.history.goBack()
                                }}
                            >
                                <View>
                                   <Icon name="left"></Icon>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </Flex.Item>
                </Flex>
            </View >

        )
    }
}

export default withRouter(GoBackBar)

