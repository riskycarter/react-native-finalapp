import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Linking, ImageBackground, TouchableOpacity } from 'react-native';
import { InputItem, Flex, Button, WingBlank, Icon } from '@ant-design/react-native';
import { withRouter } from "react-router-native";

class NavBar extends Component {

    render() {

        return (
            <View  >
                <Flex direction="row" style={{ backgroundColor: "black" }} >
                    <Flex.Item style={{ padding: 10 }} >
                        <View style={{ alignItems: "center" }}>
                            <TouchableOpacity style={{
                                width: '75%',
                                height: 36,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                            onPress={()=>{
                                this.props.history.replace('/movies')
                            }}
                            >
                                <Icon name='home' />
                            </TouchableOpacity>
                        </View>
                    </Flex.Item>
                    <Flex.Item style={{ padding: 10 }}>
                        <View style={{ alignItems: "center" }}>
                            <TouchableOpacity style={{
                                width: '75%',
                                height: 36,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                            onPress={()=>{
                                this.props.history.replace('/profile')
                            }}>
                                <Icon name='user' />
                            </TouchableOpacity>
                        </View>
                    </Flex.Item>
                </Flex>
            </View >

        )
    }
}

export default withRouter(NavBar)

