import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Button, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Tabs, WhiteSpace } from '@ant-design/react-native';
import moment from 'moment';
import axios from 'axios'


export default class HomePage extends React.Component {
    state = {
        date: new Date(),
        tomorrow: new Date(),
        products: [],
        showTimes: [],
        isLoading: true,
        
        isGetProduct: false,
    }


    UNSAFE_componentWillMount() {

        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => response.data)
            .then(data => {
                this.setState({ products: data, isGetProduct: true })
                console.log(data)
                return data
            })
            .catch(error => {
                console.log(error)
            })
            .then(data => {
                data.forEach((element, index) => {
                    console.log(element._id)
                    axios.get('https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/' + element._id, {
                        params:
                        {
                            date: this.state.date.getTime() 
                        }

                    })
                        .then(response => response.data)
                        .then(data => {
                            console.log('forEach ', data)
                            this.setState({
                                showTimes: {
                                    ...this.state.showTimes,
                                    [index]: data
                                }
                            })
                            console.log('showtimes ', this.state.showTimes, ' index ', index)
                        })
                });
                this.setState({ isLoading: false })
            }

            )

        this.setState({
            currentDate: moment(this.state.currentDate).format('DD-MM-YYYY'),
            tomorrow: moment(this.state.tomorrow).add(1, 'day').format('DD-MM-YYYY'),
            theDayAfterTomorrow: moment(this.state.theDayAfterTomorrow).add(2, 'day').format('DD-MM-YYYY')
        })



    }


    render() {
        const renderContent2 = () => {
            const style = {
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                flex: 1,
                borderRadius: 5,
            };
            const content2 = this.state.products.map((i, index) => {
                return (
                    <View key={`_${i.name}`} style={style}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
                            <View >
                                <Image style={{ width: 150, height: 200 }} source={{ uri: i.image }} />
                            </View>
                            <View style={{
                                flex: 2, marginTop: 10,
                            }}>
                                <Text style={{
                                    textAlign: 'center', fontSize: 16, fontWeight: 'bold',
                                    color: 'white'
                                }}>
                                    {i.name}
                                </Text>
                                <View style={{ marginLeft: 16 }}>
                                    <Text style={{ lineHeight: 26, paddingTop: 10, fontSize: 16, color: '#bbbbbb' }}>
                                        Duration : {i.duration + " minutes"} {'\n'}
                                        Soundtrack : {i.soundtracks + '  '} {'\n'}
                                        Subtitle : {i.subtitles + ' '} {'\n'}
                                        Showtime :
                                        {
                                            this.state.showTimes && this.state.showTimes[index] ?
                                                this.state.showTimes[index].length
                                                : null
                                        }
                                    </Text>
                                    <TouchableOpacity style={{
                                        padding: 10,
                                        backgroundColor: 'rgba(192,192,192,0.5)',
                                        borderRadius: 5,
                                        width: 100,
                                    }}
                                        onPress={() => {
                                            this.goToMovieShowtime(i)
                                        }}>
                                        <Text style={{
                                            color: 'white',
                                            textAlign: 'center'
                                        }}>View Detials</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                    </View>

                );
            });
            return <ScrollView style={{ backgroundColor: 'black' }}>{content2}</ScrollView>;
        };

        if (!this.state.isLoading) {
            if (this.state.isGetProduct) {
                return (
                    <View style={{ flex: 2 }}>
                        {renderContent2}
                    </View>
                )
                }
        }
        return <ActivityIndicator style={{ flex: 1, justifyContent: 'center' }} size="large" color="#0000ff" />


    }

}
