import React, { Component, Fragment } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import store from './AppStore'
import HomePage from './containers/HomePage'
import ProfilePage from './containers/ProfilePage'
import LoginPage from './containers/LoginPage'
import SignupPage from './containers/RegisterPage'
import MoviePage from './containers/MoviePage'
import CinemaPage from './containers/CinemaPage'
import EditProfilePage from './containers/EditProfile'
import BookingListPage from './containers/BookingListPage'
import ChangePassword from './containers/ChangePassword'

class Router extends Component {

    render() {
        return (
            <Provider store={store}>
                <NativeRouter>
                    <Switch>
                        <Route exact path='/login' component={LoginPage} />
                        <Route exact path='/' component={LoginPage} />
                        <Route exact path='/movies' component={HomePage} />
                        <Route exact path='/signup' component={SignupPage} />
                        <Route exact path='/profile' component={ProfilePage} />
                        <Route exact path='/movie' component={MoviePage} />
                        <Route exact path='/cinema' component={CinemaPage} />
                        <Route exact path='/editprofile' component={EditProfilePage} />
                        <Route exact path='/bookinglist' component={BookingListPage} />
                        <Route exact path='/changepassword' component={ChangePassword} />
                    </Switch>
                </NativeRouter>
            </Provider>


        )
    }

}

export default Router
